#
# User configuration sourced by all invocations of the shell
#

# Define Zim location
ZIM_HOME=${ZDOTDIR:-${HOME}}/.zim

typeset -U path
path=(~/.local/bin $path)
path=(~/.npm-global/bin $path)

export VISUAL=nano
export EDITOR=nano
export MOZ_USE_XINPUT2=1
export _JAVA_AWT_WM_NONREPARENTING=1
